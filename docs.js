import React from 'react';
import { useDispatch } from 'react-redux';
import {update} from '../features/ProductSlice'
function AddProduct() {
    const [items,setItems] = React.useState({
        title:"",
        price:''
    })

    const dispatch = useDispatch()

    function handleChange(e){
        const {name, value} = e.target
        console.log(name)

       setItems(prevValue=>{
        const updateValue = {...prevValue, [name]:value}
        return updateValue
       })
    }

    function updateProduct(e){
        e.preventDefault();
        dispatch(update({title : items.title, price : items.price}))
    }

  return (
    <div>
     <form onSubmit={updateProduct} className="box mt-5">
        <div className="field">
            <label htmlFor="" className="label">Title</label>
                <div className="control">
                    <input 
                    type="text" 
                    className='input' 
                    placeholder='Title'
                    name='title'
                    value={items.title}
                    onChange={handleChange}
                    />
                </div>
        </div>
        <div className="field">
            <label htmlFor="" className="label">Price</label>
                <div className="control">
                    <input 
                    type="text" 
                    className='input' 
                    placeholder='Price' 
                    name='price'
                    value={items.price}
                    onChange={handleChange}
                    />
                </div>
        </div>
        <div className="field">
            <button className='button is-success'>Submit</button>
        </div>
     </form>
    </div>
  )
}

export default AddProduct