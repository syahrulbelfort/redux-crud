import { createSlice, createAsyncThunk, createEntityAdapter } from "@reduxjs/toolkit"; 
import axios from "axios";

///DI BAWAH INI NAMANYA ACTION 
export const getProducts = createAsyncThunk('product/getProducts', async ()=>{
    const response = await axios.get('http://localhost:3000/product')
    return response.data
});
export const saveProduct = createAsyncThunk('product/saveProduct', async ({title,price})=>{
    const response = await axios.post('http://localhost:3000/product',{
        title,
        price
    })
    return response.data
})
export const updateProduct = createAsyncThunk('product/updateProduct', async ({id,title,price})=>{
    const response = await axios.patch(`http://localhost:3000/product/${id}`,{
        title,
        price
    })
    return response.data
})

export const deleteProduct = createAsyncThunk('product/deleteproduct', async (id)=>{
    await axios.delete(`http://localhost:3000/product/${id}`)
    return id
})

export const productEntity = createEntityAdapter({
    selectId: (product) => product.id
})

console.log(productEntity)

///DI ATAS INI NAMANYA ACTION 


const productSlice = createSlice({
    name: "product",
    initialState: productEntity.getInitialState(),
    extraReducers: {
        [getProducts.fulfilled]: (state, action) => {
            productEntity.setAll(state, action.payload)
        },
        [saveProduct.fulfilled]: (state, action) => {
            productEntity.addOne(state, action.payload)
        },
        [deleteProduct.fulfilled]: (state, action) => {
            productEntity.removeOne(state, action.payload)
        },
        [updateProduct.fulfilled]: (state, action) => {
            productEntity.updateOne(state,{id : action.payload.id, updates: action.payload})
        }
    }
})

export const productSelectors = productEntity.getSelectors(state=> state.product)
export default productSlice.reducer