import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { getProducts, productSelectors, updateProduct } from '../features/ProductSlice';

function UpdateProduct() {
  const [items, setItems] = useState({
    title: '',
    price: '',
  });

  const { id } = useParams();
  const product = useSelector((state) => productSelectors.selectById(state, id));
  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getProducts());
  }, [dispatch]);

  useEffect(() => {
    if (product) {
      setItems({
        ...items,
        title: product.title,
        price: product.price,
      });
    }
  }, [product, setItems,items]);

  function handleChange(e) {
    const { name, value } = e.target;

    setItems((prevValue) => ({
      ...prevValue,
      [name]: value,
    }));
  }

  function handleSubmit(e) {
    e.preventDefault();
    dispatch(updateProduct({ id, ...items }));
    navigate('/');
  }

  return (
    <div>
      <form onSubmit={handleSubmit} className="box mt-5">
        <div className="field">
          <label htmlFor="title" className="label">
            Title
          </label>
          <div className="control">
            <input
              type="text"
              className="input"
              placeholder="Title"
              name="title"
              id="title"
              value={items.title}
              onChange={handleChange}
            />
          </div>
        </div>
        <div className="field">
          <label htmlFor="price" className="label">
            Price
          </label>
          <div className="control">
            <input
              type="text"
              className="input"
              placeholder="Price"
              name="price"
              id="price"
              value={items.price}
              onChange={handleChange}
            />
          </div>
        </div>
        <div className="field">
          <button className="button is-success" type="submit">
            Update
          </button>
        </div>
      </form>
    </div>
  );
}

export default UpdateProduct;
