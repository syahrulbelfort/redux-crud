import React from 'react'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { saveProduct } from '../features/ProductSlice'

function AddProduct() {
    const [item, setItem] = React.useState({
        title:'',
        price:''
    })
    const dispatch = useDispatch()
    const navigate = useNavigate()
    
  async  function handleSubmit(e){
        e.preventDefault()
        await dispatch(saveProduct({title : item.title, price: item.price}))
        navigate('/')
    }

    const handleChange = (e) =>{
        const {name,value} = e.target
        setItem((prev)=>{
            const updatedValue = {
                ...prev, [name] : value
            }
            return updatedValue
        })
    } 

  return (
    <div>
  <form onSubmit={handleSubmit}  className="box mt-5">
        <div className="field">
            <label htmlFor="" className="label">Title</label>
                <div className="control">
                    <input 
                    type="text" 
                    className='input' 
                    placeholder='Title'
                    name='title'
                    value={item.title}
                    onChange={handleChange}
                    />
                </div>
        </div>
        <div className="field">
            <label htmlFor="" className="label">Price</label>
                <div className="control">
                    <input 
                    type="text" 
                    className='input' 
                    placeholder='Price' 
                    name='price'
                    value={item.price}
                    onChange={handleChange}
                    />
                </div>
        </div>
        <div className="field">
            <button className='button is-success'>Update</button>
        </div>
     </form>
    </div>  )
}

export default AddProduct